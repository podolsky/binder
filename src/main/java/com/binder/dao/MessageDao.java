package com.binder.dao;

import com.binder.entity.Message;

public interface MessageDao extends GeneralDao<Message> {

}
