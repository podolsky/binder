### Binder
Binder Dating Web service

### Tools
* Maven
* Jetty
* JUnit
* Servlets
* JDBC API

### Tasks (duplicates in issues)
1) Implement all DAOs with JDBC (Yuri Podolsky)     
2) Create database structure (DDL) (Yuri Podolsky)     
3) Make Freemarker (Ivan Durmanenko)     
4) Make Assert Servlet (Ivan Durmanenko)     
5) Implement all servlets:     
* 5.1 Login, Logout, AuthFilter (Yuri Pastushyna)     
* 5.2 Main, Match, Chat (Ivan Durmanenko)   

6) Add config to POM.xml to enable execute jar from console (Yuri Podolsky, Yuri Pastushyna)   
7) Implement auth filter (Yuri Pastushyna)   
8) Add bootstrap and styles css (Ivan Durmanenko)    
9) Deploy to AWS (Yuri Pastushyna)   