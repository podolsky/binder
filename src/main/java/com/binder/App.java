package com.binder;

import com.binder.servlet.LoginServlet;
import com.binder.servlet.MainServlet;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

public class App {
    public static void main(String[] args) throws Exception {
        Server server = new Server(8080);
        ServletContextHandler context = new ServletContextHandler();
        server.setHandler(context);
        context.addServlet(new ServletHolder(new MainServlet()), "/*");
        context.addServlet(new ServletHolder(new LoginServlet()), "/login");
        server.start();
        server.join();
    }
}
