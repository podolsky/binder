package com.binder.dao;

import com.binder.DatabaseConfig;

import java.util.List;

public abstract class AbstractDao<T> implements GeneralDao<T>{

    private Class<T> clazz;
    private DatabaseConfig databaseConfig;

    public AbstractDao(Class<T> clazz, DatabaseConfig databaseConfig) {
        this.clazz = clazz;
        this.databaseConfig = databaseConfig;
    }

    @Override
    public void save(T entity) {

    }

    @Override
    public T get(Long id) {
        return null;
    }

    @Override
    public List<T> getAll(Long offset, Long pageSize) {
        return null;
    }

    @Override
    public void update(T entity) {

    }

    @Override
    public void remove(T entity) {

    }
}
