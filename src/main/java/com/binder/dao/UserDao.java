package com.binder.dao;

import com.binder.entity.User;

import java.util.Set;

public interface UserDao extends GeneralDao<User> {

    Set<User> findMatchedUsers(Long offset, Long pageSize, Long currentUserId);
    User findNextUser(Long currentUserId);


}
