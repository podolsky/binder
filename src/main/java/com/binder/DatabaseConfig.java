package com.binder;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DatabaseConfig {
    private static Connection conn;

    private DatabaseConfig() throws IOException, SQLException {
        Properties prop = new Properties();
        InputStream in = new FileInputStream("db.properties");
        prop.load(in);
        in.close();
        String password = prop.getProperty("jdbc.password");
        String username = prop.getProperty("jdbc.username");
        String url = prop.getProperty("jdbc.url");
        conn = DriverManager.getConnection(url, username, password);
    }

    public Connection getConnection() {
        return conn;
    }
}
