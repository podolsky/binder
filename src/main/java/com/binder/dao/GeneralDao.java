package com.binder.dao;

import java.util.List;

public interface GeneralDao<T> {
    void save(T entity);
    T get(Long id);
    List<T> getAll(Long offset, Long pageSize);
    void update(T entity);
    void remove(T entity);
}
