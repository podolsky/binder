package com.binder.dao;

import com.binder.DatabaseConfig;
import com.binder.entity.User;

import java.io.FileInputStream;
import java.util.List;
import java.util.Properties;


public class UserDaoImpl extends AbstractDao<User> implements UserDao {

    public UserDaoImpl(DatabaseConfig databaseConfig) {
        super(User.class, databaseConfig);
    }

}
